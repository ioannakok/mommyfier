import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MommyfierTest {

    @Test
    void shouldReturnAnEmptyString() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("", mommyfier.mommify(""));
    }

    @Test
    void shouldReturnAConsonant() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("c", mommyfier.mommify("c"));
    }

    @Test
    void shouldReturnMommyForASingleA() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("a"));
    }

    @Test
    void shouldReturnMommyForASingleE() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("e"));
    }

    @Test
    void shouldReturnMommyForASingleI() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("i"));
    }

    @Test
    void shouldReturnMommyForASingleO() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("o"));
    }

    @Test
    void shouldReturnMommyForASingleU() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("u"));
    }

    @Test
    void shouldReturnMommyForDoubleA() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("aa"));
    }

    @Test
    void shouldReturnMommyForDoubleE() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("ee"));
    }

    @Test
    void shouldReturnMommyForDoubleI() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("ii"));
    }

    @Test
    void shouldReturnMommyForTripleO() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("mommy", mommyfier.mommify("ooo"));
    }

    @Test
    void shouldReturnWordForHard() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("hard", mommyfier.mommify("hard"));
    }


    @Test
    void shouldReturnBmommytForBat() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("bmommyt", mommyfier.mommify("bat"));

    }

    @Test
    void shouldReturnBmommygmommynForBegin() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("bmommygmommyn", mommyfier.mommify("begin"));
    }

    @Test
    void shouldReturnHmommyppmommyForHaeippyoi() {
        Mommyfier mommyfier = new Mommyfier();
        assertEquals("hmommyppymommy", mommyfier.mommify("haeippyoi"));
    }

}
