import java.util.Arrays;
import java.util.stream.Collectors;

public class Mommyfier {

    public String mommify(String input) {

        return Arrays.asList(new String[] {input}).stream()
                .map(word -> (((double)word.toLowerCase().chars()
                        .filter(c -> Character.toString((char)c).matches("[a|e|i|o|u]")).count() / word.length() * 100) > 30) ?
                        word.replaceAll("[a|e|i|o|u]+", "mommy") : word)
                .collect(Collectors.toList()).get(0);
    }
}
